﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exo_1._1
{
    class Program
    {
        static void Main(string[] args)
        {
            int varA = 1;
            int varB = 500;
            int varTemp = 0;

            Console.WriteLine($"la variable A est =  {varA} La variable B = {varB}");

            varTemp = varA;
            varA = varB;
            varB = varTemp;
            varTemp =0;

            Console.WriteLine($"la variable A est = a {varA} La variable B = {varB}");

            Console.ReadKey();


        }
    }
}
