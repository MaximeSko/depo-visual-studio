﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exo3._3
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 1; i <= 20; i++)
            {
                Console.WriteLine(i);
                for (int c = 0; c <= 20; c++)
                {
                    Console.WriteLine($"{i} X {c} = {i*c}");
                    
                }
            }
            Console.ReadKey();
        }
    }
}
