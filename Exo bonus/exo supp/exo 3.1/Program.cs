﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exo_3._1
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] tabMots = new string[5] { "Bonjour", "ceci", "est", "un", "test" };

            for (int i= 0; i<= 4; i++)
            {
                Console.WriteLine(tabMots[i]);
            }

            Console.WriteLine("");

            foreach (string v in tabMots)
            {
                Console.WriteLine(v);
            }

            Console.ReadKey();
        }
    }
}
