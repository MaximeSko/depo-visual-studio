﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace exo2._1
{
    public partial class Form1 : Form
    {
        public static int number1 = 0;
        public static int number2 = 0;
        public static string operateur = "0";
        public static int result = 0;
        public Form1()
        {
            InitializeComponent();
           

        }

        public void buttonNumber1_Click(object sender, EventArgs e)
        {
            if (operateur == "0")
            {

                number1 = (number1 + 1);
                label1.Text = number1.ToString();
            }
            else
            {
                number2 = (number2 + 1);
                label2.Text = number2.ToString();
            }

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void buttonNumber2_Click(object sender, EventArgs e)
        {
            if (operateur == "0")
            {

                number1 = (number1 + 2);
                label1.Text = number1.ToString();
            }
            else
            {
                number2 = (number2 + 2);
                label2.Text = number2.ToString();
            }
        }

        private void buttonNumber3_Click(object sender, EventArgs e)
        {
            if (operateur == "0")
            {

                number1 = (number1 + 3);
                label1.Text = number1.ToString();
            }
            else
            {
                number2 = (number2 + 3);
                label2.Text = number2.ToString();
            }
        }

        private void buttonNumber0_Click(object sender, EventArgs e)
        {
            if (operateur == "0")
            {

                number1 = (number1 + 0);
                label1.Text = number1.ToString();
            }
            else
            {
                number2 = (number2 + 0);
                label2.Text = number2.ToString();
            }
        }

        private void buttonNumber4_Click(object sender, EventArgs e)
        {
            if (operateur == "0")
            {

                number1 = (number1 + 4);
                label1.Text = number1.ToString();
            }
            else
            {
                number2 = (number2 + 4);
                label2.Text = number2.ToString();
            }
        }

        private void buttonNumber5_Click(object sender, EventArgs e)
        {
            if (operateur == "0")
            {

                number1 = (number1 + 5);
                label1.Text = number1.ToString();
            }
            else
            {
                number2 = (number2 + 5);
                label2.Text = number2.ToString();
            }
        }

        private void buttonNumber6_Click(object sender, EventArgs e)
        {
            if (operateur == "0")
            {

                number1 = (number1 + 6);
                label1.Text = number1.ToString();
            }
            else
            {
                number2 = (number2 + 6);
                label2.Text = number2.ToString();
            }
        }

        private void buttonNumber7_Click(object sender, EventArgs e)
        {
            if (operateur == "0")
            {

                number1 = (number1 + 7);
                label1.Text = number1.ToString();
            }
            else
            {
                number2 = (number2 + 7);
                label2.Text = number2.ToString();
            }
        }

        private void buttonNumber8_Click(object sender, EventArgs e)
        {
            if (operateur == "0")
            {

                number1 = (number1 + 8);
                label1.Text = number1.ToString();
            }
            else
            {
                number2 = (number2 + 8);
                label2.Text = number2.ToString();
            }
        }

        private void buttonNumber9_Click(object sender, EventArgs e)
        {
            if (operateur == "0")
            {

                number1 = (number1 + 9);
                label1.Text = number1.ToString();
            }
            else
            {
                number2 = (number2 + 9);
                label2.Text = number2.ToString();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            operateur = "x";
            label3.Text = "X";
        }

        private void buttonOperateurPlus_Click(object sender, EventArgs e)
        {
            operateur = "+";
            label3.Text = "+";
        }

        private void buttonOperateurMoins_Click(object sender, EventArgs e)
        {
            operateur = "-";
            label3.Text = "-";
        }

        private void buttonOperateurDivi_Click(object sender, EventArgs e)
        {
            operateur = "/";
            label3.Text = "/";
        }

        private void buttonEnter_Click(object sender, EventArgs e)
        {
            switch (operateur)
            {
                case "+":
                    result = (number1 + number2);
                    label4.Text = result.ToString();
                    break;

                case "-":
                    result = (number1 - number2);
                    label4.Text = result.ToString();
                    break;

                case "x":
                    result = (number1 * number2);
                    label4.Text = result.ToString();
                    break;

                case "/":
                    if (number1 > number2)
                    {
                        result = (number1 / number2);
                        label4.Text = result.ToString();
                       
                    }
                    else
                    {
                        MessageBox.Show($"{number1.ToString()} Non Divisible par {number2.ToString()} ");
                        number1 = 0;
                        number2 = 0;
                        operateur = "0";
                        result = 0;
                        label1.Text = "";
                        label2.Text = "";
                        label3.Text = "";
                        label4.Text = "";
                    }
                    break;



            }
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
          number1 = 0;
          number2 = 0;
          operateur = "0";
           result = 0;
            label1.Text = "";
            label2.Text = "";
            label3.Text = "";
            label4.Text = "";
        }
    }
}