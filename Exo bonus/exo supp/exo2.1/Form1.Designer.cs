﻿namespace exo2._1
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonNumber1 = new System.Windows.Forms.Button();
            this.buttonNumber2 = new System.Windows.Forms.Button();
            this.buttonNumber3 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonNumber0 = new System.Windows.Forms.Button();
            this.buttonNumber4 = new System.Windows.Forms.Button();
            this.buttonNumber6 = new System.Windows.Forms.Button();
            this.buttonNumber5 = new System.Windows.Forms.Button();
            this.buttonNumber8 = new System.Windows.Forms.Button();
            this.buttonNumber7 = new System.Windows.Forms.Button();
            this.buttonNumber9 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonOperateurPlus = new System.Windows.Forms.Button();
            this.buttonOperateurMoins = new System.Windows.Forms.Button();
            this.buttonOperateurX = new System.Windows.Forms.Button();
            this.buttonOperateurDivi = new System.Windows.Forms.Button();
            this.buttonEnter = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonReset = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonNumber1
            // 
            this.buttonNumber1.Location = new System.Drawing.Point(12, 120);
            this.buttonNumber1.Name = "buttonNumber1";
            this.buttonNumber1.Size = new System.Drawing.Size(36, 23);
            this.buttonNumber1.TabIndex = 0;
            this.buttonNumber1.Text = "1";
            this.buttonNumber1.UseVisualStyleBackColor = true;
            this.buttonNumber1.Click += new System.EventHandler(this.buttonNumber1_Click);
            // 
            // buttonNumber2
            // 
            this.buttonNumber2.Location = new System.Drawing.Point(12, 149);
            this.buttonNumber2.Name = "buttonNumber2";
            this.buttonNumber2.Size = new System.Drawing.Size(36, 23);
            this.buttonNumber2.TabIndex = 1;
            this.buttonNumber2.Text = "2";
            this.buttonNumber2.UseVisualStyleBackColor = true;
            this.buttonNumber2.Click += new System.EventHandler(this.buttonNumber2_Click);
            // 
            // buttonNumber3
            // 
            this.buttonNumber3.Location = new System.Drawing.Point(12, 178);
            this.buttonNumber3.Name = "buttonNumber3";
            this.buttonNumber3.Size = new System.Drawing.Size(36, 23);
            this.buttonNumber3.TabIndex = 2;
            this.buttonNumber3.Text = "3";
            this.buttonNumber3.UseVisualStyleBackColor = true;
            this.buttonNumber3.Click += new System.EventHandler(this.buttonNumber3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "label1";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // buttonNumber0
            // 
            this.buttonNumber0.Location = new System.Drawing.Point(12, 89);
            this.buttonNumber0.Name = "buttonNumber0";
            this.buttonNumber0.Size = new System.Drawing.Size(36, 25);
            this.buttonNumber0.TabIndex = 4;
            this.buttonNumber0.Text = "0";
            this.buttonNumber0.UseVisualStyleBackColor = true;
            this.buttonNumber0.Click += new System.EventHandler(this.buttonNumber0_Click);
            // 
            // buttonNumber4
            // 
            this.buttonNumber4.Location = new System.Drawing.Point(12, 207);
            this.buttonNumber4.Name = "buttonNumber4";
            this.buttonNumber4.Size = new System.Drawing.Size(36, 23);
            this.buttonNumber4.TabIndex = 5;
            this.buttonNumber4.Text = "4";
            this.buttonNumber4.UseVisualStyleBackColor = true;
            this.buttonNumber4.Click += new System.EventHandler(this.buttonNumber4_Click);
            // 
            // buttonNumber6
            // 
            this.buttonNumber6.Location = new System.Drawing.Point(68, 120);
            this.buttonNumber6.Name = "buttonNumber6";
            this.buttonNumber6.Size = new System.Drawing.Size(36, 23);
            this.buttonNumber6.TabIndex = 6;
            this.buttonNumber6.Text = "6";
            this.buttonNumber6.UseVisualStyleBackColor = true;
            this.buttonNumber6.Click += new System.EventHandler(this.buttonNumber6_Click);
            // 
            // buttonNumber5
            // 
            this.buttonNumber5.Location = new System.Drawing.Point(68, 89);
            this.buttonNumber5.Name = "buttonNumber5";
            this.buttonNumber5.Size = new System.Drawing.Size(36, 23);
            this.buttonNumber5.TabIndex = 7;
            this.buttonNumber5.Text = "5";
            this.buttonNumber5.UseVisualStyleBackColor = true;
            this.buttonNumber5.Click += new System.EventHandler(this.buttonNumber5_Click);
            // 
            // buttonNumber8
            // 
            this.buttonNumber8.Location = new System.Drawing.Point(68, 178);
            this.buttonNumber8.Name = "buttonNumber8";
            this.buttonNumber8.Size = new System.Drawing.Size(36, 23);
            this.buttonNumber8.TabIndex = 8;
            this.buttonNumber8.Text = "8";
            this.buttonNumber8.UseVisualStyleBackColor = true;
            this.buttonNumber8.Click += new System.EventHandler(this.buttonNumber8_Click);
            // 
            // buttonNumber7
            // 
            this.buttonNumber7.Location = new System.Drawing.Point(68, 149);
            this.buttonNumber7.Name = "buttonNumber7";
            this.buttonNumber7.Size = new System.Drawing.Size(36, 23);
            this.buttonNumber7.TabIndex = 9;
            this.buttonNumber7.Text = "7";
            this.buttonNumber7.UseVisualStyleBackColor = true;
            this.buttonNumber7.Click += new System.EventHandler(this.buttonNumber7_Click);
            // 
            // buttonNumber9
            // 
            this.buttonNumber9.Location = new System.Drawing.Point(68, 207);
            this.buttonNumber9.Name = "buttonNumber9";
            this.buttonNumber9.Size = new System.Drawing.Size(36, 23);
            this.buttonNumber9.TabIndex = 10;
            this.buttonNumber9.Text = "9";
            this.buttonNumber9.UseVisualStyleBackColor = true;
            this.buttonNumber9.Click += new System.EventHandler(this.buttonNumber9_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(142, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "label2";
            // 
            // buttonOperateurPlus
            // 
            this.buttonOperateurPlus.Location = new System.Drawing.Point(122, 89);
            this.buttonOperateurPlus.Name = "buttonOperateurPlus";
            this.buttonOperateurPlus.Size = new System.Drawing.Size(75, 23);
            this.buttonOperateurPlus.TabIndex = 12;
            this.buttonOperateurPlus.Text = "+";
            this.buttonOperateurPlus.UseVisualStyleBackColor = true;
            this.buttonOperateurPlus.Click += new System.EventHandler(this.buttonOperateurPlus_Click);
            // 
            // buttonOperateurMoins
            // 
            this.buttonOperateurMoins.Location = new System.Drawing.Point(122, 120);
            this.buttonOperateurMoins.Name = "buttonOperateurMoins";
            this.buttonOperateurMoins.Size = new System.Drawing.Size(75, 23);
            this.buttonOperateurMoins.TabIndex = 13;
            this.buttonOperateurMoins.Text = "-";
            this.buttonOperateurMoins.UseVisualStyleBackColor = true;
            this.buttonOperateurMoins.Click += new System.EventHandler(this.buttonOperateurMoins_Click);
            // 
            // buttonOperateurX
            // 
            this.buttonOperateurX.Location = new System.Drawing.Point(122, 149);
            this.buttonOperateurX.Name = "buttonOperateurX";
            this.buttonOperateurX.Size = new System.Drawing.Size(75, 23);
            this.buttonOperateurX.TabIndex = 14;
            this.buttonOperateurX.Text = "x";
            this.buttonOperateurX.UseVisualStyleBackColor = true;
            this.buttonOperateurX.Click += new System.EventHandler(this.button3_Click);
            // 
            // buttonOperateurDivi
            // 
            this.buttonOperateurDivi.Location = new System.Drawing.Point(122, 178);
            this.buttonOperateurDivi.Name = "buttonOperateurDivi";
            this.buttonOperateurDivi.Size = new System.Drawing.Size(75, 23);
            this.buttonOperateurDivi.TabIndex = 15;
            this.buttonOperateurDivi.Text = "/";
            this.buttonOperateurDivi.UseVisualStyleBackColor = true;
            this.buttonOperateurDivi.Click += new System.EventHandler(this.buttonOperateurDivi_Click);
            // 
            // buttonEnter
            // 
            this.buttonEnter.Location = new System.Drawing.Point(122, 207);
            this.buttonEnter.Name = "buttonEnter";
            this.buttonEnter.Size = new System.Drawing.Size(75, 23);
            this.buttonEnter.TabIndex = 16;
            this.buttonEnter.Text = "Enter";
            this.buttonEnter.UseVisualStyleBackColor = true;
            this.buttonEnter.Click += new System.EventHandler(this.buttonEnter_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(81, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "label3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(243, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "label4";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(183, 15);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(43, 18);
            this.button1.TabIndex = 19;
            this.button1.Text = "=";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // buttonReset
            // 
            this.buttonReset.Location = new System.Drawing.Point(218, 89);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(60, 54);
            this.buttonReset.TabIndex = 20;
            this.buttonReset.Text = "Reset";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(288, 261);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buttonEnter);
            this.Controls.Add(this.buttonOperateurDivi);
            this.Controls.Add(this.buttonOperateurX);
            this.Controls.Add(this.buttonOperateurMoins);
            this.Controls.Add(this.buttonOperateurPlus);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonNumber9);
            this.Controls.Add(this.buttonNumber7);
            this.Controls.Add(this.buttonNumber8);
            this.Controls.Add(this.buttonNumber5);
            this.Controls.Add(this.buttonNumber6);
            this.Controls.Add(this.buttonNumber4);
            this.Controls.Add(this.buttonNumber0);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonNumber3);
            this.Controls.Add(this.buttonNumber2);
            this.Controls.Add(this.buttonNumber1);
            this.Name = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonNumber1;
        private System.Windows.Forms.Button buttonNumber2;
        private System.Windows.Forms.Button buttonNumber3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonNumber0;
        private System.Windows.Forms.Button buttonNumber4;
        private System.Windows.Forms.Button buttonNumber6;
        private System.Windows.Forms.Button buttonNumber5;
        private System.Windows.Forms.Button buttonNumber8;
        private System.Windows.Forms.Button buttonNumber7;
        private System.Windows.Forms.Button buttonNumber9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonOperateurPlus;
        private System.Windows.Forms.Button buttonOperateurMoins;
        private System.Windows.Forms.Button buttonOperateurX;
        private System.Windows.Forms.Button buttonOperateurDivi;
        private System.Windows.Forms.Button buttonEnter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonReset;
    }
}

