﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace exo_4_v1
{
    class Program
    {
        // Bonus: Sauvegarde dans un fichier 
        // dans une boucle
        // demander à l'utilisateur de pour ajouter une valeur clé dans ce dictionnaire
        // demander à l'utilisateur de spcécifier la quantité
        // afficher l'état du stock

        static void Main(string[] args)
        {


            Dictionary<string, int> bar = new Dictionary<string, int>();

            string fichier = @"bieres.csv";

            if (!File.Exists(fichier))
            {
                FileStream f = File.Create(fichier);
                f.Close();
            }

            while (true)
            {
                
                Console.WriteLine("1.Ajouter");
                Console.WriteLine("2.Modifier");
                Console.WriteLine("3.Supprimer");
                Console.WriteLine("4.Sauvegarder");


                int choix = int.Parse(Console.ReadLine());

                switch (choix)
                {
                    case 1:
                        Console.Clear();
                        Console.WriteLine("entré une boisson a ajouter ");
                        string userInKey = Console.ReadLine();

                        userInKey = userInKey.ToUpper();

                        while (bar.ContainsKey(userInKey))
                        {
                            Console.WriteLine("Le produit existe deja !");

                            Console.WriteLine("entré une boisson a ajouter ");
                            userInKey = Console.ReadLine();
                        }

                        Console.WriteLine("Quel quantité");
                        string userInValue = Console.ReadLine();
                        int nb;

                        while (!int.TryParse(userInValue, out nb))
                        {
                            Console.WriteLine("Quantité non valide");
                            userInValue = Console.ReadLine();
                        }

                        bar.Add(userInKey, nb);
                        Console.Clear();

                        break;

                    case 2:
                        Console.Clear();
                        foreach (KeyValuePair<string, int> kvp in bar)
                        {
                            Console.WriteLine($"{kvp.Key} : {kvp.Value}");
                        }

                        Console.WriteLine("Quel Boisson voullez vous modifier ?");
                        string modiferKey = Console.ReadLine();
                        Console.WriteLine("Quel stock ?");
                        int modifierValue = int.Parse(Console.ReadLine());

                        bar[modiferKey.ToUpper()] = modifierValue;
                        Console.Clear();
                        Console.WriteLine("modifer");

                        break;
                    case 3:
                        Console.Clear();
                        foreach (KeyValuePair<string, int> kvp in bar)
                        {
                            Console.WriteLine($"{kvp.Key} : {kvp.Value}");
                        }

                        Console.WriteLine("Quel Boisson voullez vous Supprimer ?");
                         modiferKey = Console.ReadLine();
                        bar.Remove(modiferKey.ToUpper());
                        Console.Clear();
                        Console.WriteLine($"{modiferKey.ToUpper()} Supprimer !");
                        break;
                    case 4:
                        Console.Clear();
                        List<string> barListSave = new List<string>();

                        foreach (KeyValuePair<string, int> kvp in bar)
                        {
                            barListSave.Add($"{kvp.Key};{kvp.Value}");
                        }

                        File.WriteAllLines(fichier, barListSave);

                        Console.WriteLine("Sauvegarder !");
                        break;
                    default:
                        Console.WriteLine("choix incorect");
                        break;


                }
            

                foreach(KeyValuePair<string,int> kvp in bar)
                {
                    Console.WriteLine($"{kvp.Key} : {kvp.Value}");
                }




            }
        }
    }
}
