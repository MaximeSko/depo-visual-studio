﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace introduction_vs_Csharp
{
    class Program
    {
        static void Main(string[] args)
        {
            // initialisation des variables
            int userIn = 0;
            int minutes = 0;
            int heures = 0;

            //demande de saisie par l'utilisateur
            Console.WriteLine("Saisire un nombre de seconde");
            //convertion d'une str en int et recuperation de la saisie 
            userIn = (int.Parse(Console.ReadLine()));
            // calcule
           heures = userIn / 3600;
           userIn = userIn % 3600;
           minutes = userIn / 60;
           userIn = userIn % 60;

            //affichage
            Console.WriteLine($"{userIn}  fait {heures} heures, {minutes} minutes {userIn} secondes");


            //stop pour pouvoir voir
            Console.ReadKey();

        }
    }
}
